from inspect import currentframe, getframeinfo

cf = currentframe()
filename = getframeinfo(cf).filename

# print(dir(cf))
for attr in [i for i in dir(cf) if "__"  not in i]:
    print("*"*5, attr, "*"*5, getattr(cf, attr))

print("This is line 5, python says line ", cf.f_lineno)
print("The filename is ", filename)