# def foo():
#     a = yield
#     b = yield
#     yield a * b
#
# def r(x):
#     for i in range(x):
#         yield i
#
# gen = foo()
# next(gen)
# gen.send(10)
# result = gen.send(15)
# print(result)
#
# b = r(1_000_000)
# print(next(b))
# print(next(b))
# print(next(b))
# print(next(b))

from itertools import zip_longest


a = [1, 2, 3, 4, 5, 6]
print(*zip(*[iter(a)]*2))
zip(iter(a), iter(a))