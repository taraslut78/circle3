from flask import Flask, request, render_template

app = Flask("My first Flask Application")


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/user/<username_id>")
def user(username_id):

    return render_template("index_content.html", name=username_id)


@app.route("/users")
def users():
    data = [{"name":"Newton", "text":"text1"},
            {"name":"Einshtain", "text":"Hello "}]
    return render_template("index_for.html", items=data)



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8888)
