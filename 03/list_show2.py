ll = [1225, 12, -5, 2]

print(ll)

ll.append(0)
print(ll)

ll.insert(0, 12)
print(ll)

print(ll.index(12, 1))

rez = ll.pop(0)
print(rez)
print(ll)

ll[4] = 2019 # no error --- list is mutable object
print(ll)

ll.append("Hello")
print(ll)

ll[0] += 12
print(ll)

ll += [12] # simple append
print(ll)

print(ll.count(0))

ll2 = list("Hello world!")
print(ll2)

# for letter in "abcdefghijklmnopqrstuvwxyz":
#     print(f"{letter}: {ll2.count(letter)}", end=",  ")
ll.pop(len(ll)-2)

# print(ll.sort()) # sorted
print(sorted(ll, reverse=True))
print(ll)

print(120 in ll)

ll += ["Hello"]
print(ll)