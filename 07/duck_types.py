def count_and_call(foo, n):
    for i in range(n):
        foo(i)


count_and_call(print, 4)


x = []

count_and_call(x.append, 5)
print(x)
