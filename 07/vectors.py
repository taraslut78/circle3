class Vector2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __getitem__(self, item):
        if item == 0:
            return self.x
        elif item == 1:
            return self.y
        else:
            raise IndexError

    def __setitem__(self, index, value):
        if index == 0:
            self.x = value
        elif index == 1:
            self.y = value
        else:
            raise IndexError

    def __call__(self, *args, **kwargs):
        for i in args:
            print(i, end=", ")

        for k in kwargs:
            print(f"{k} = {kwargs[k]}", end=", ")

        return "I am in the call method"

    def __str__(self):
        return "Vector2D<" + str(self.x) +\
               ", " + str(self.y) + ">"


if __name__ == "__main__":
    vek1 = Vector2D(2, 4)
    print('vek1=', vek1)

    vek2 = Vector2D(y=12)
    print('vek2=', vek2)

    vek3 = vek1 + vek2
    print('vek3=', vek3)

    print(vek3 == vek3)

    print(vek3[0], vek3[1])
    try:
        vek3[1] = 5
    except Exception:
        print("There are some ERROR!!!")

    print(vek3(1, 23, a=2))
