from pprint import pprint
import urllib3
from bs4 import BeautifulSoup


http = urllib3.PoolManager()
r = http.request("GET", "http://quotes.toscrape.com/")

# print([item for item in range(4) if item % 2 == 0])

# print([i for i in dir(r) if "__" not in i])
print(r.status)
# print(r.data)
# print(r.headers)
soup = BeautifulSoup(r.data, 'html.parser')
print("Title:")
print(soup.title)
# pprint([item for item in dir(soup) if "__" not in item ])

# print(soup.text)

rez = soup.find_all("div", attrs={"class":"quote"})
print(len(rez))
for i, quote in enumerate(rez):
    main_text = quote.find("span", attrs={'class':'text'})

    print(i, main_text.text[1:-1])
