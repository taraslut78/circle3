
def my(i):
    ret = 0
    while ret < i:
        yield ret
        ret +=1


for z in my(5):
    print(z)
