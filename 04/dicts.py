dd = {}
dd[1] = "Hello"
dd['word'] = 2

print(dd)

print(len(dd))

print("KEeys:" + str(dd.keys()))

dd2 = {"Circle":5, (1,2): [1,56]}

print(dd2)

dd.update(dd2)

print(dd)

#1
for key in dd.keys():
    print(key, ":", dd[key])
print("*"*30, "Dict1", "*"*30) # ctr  + d
#2
for key, value in dd.items():
    print(key, ":", value),

print("*"*50) # ctr  + shift +  UpKey or DownKey

print(tuple(dd.items()))

d3 = dict(tuple(dd.items()))
print(d3)
