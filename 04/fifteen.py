def print_greeting():
    print("Hello")


def initialize_bord():
    """
    :return: list --> [15,14, ...., 3, 1, 2, 0]
    """
    return []


def check(bord:list):
    """
    :param bord: [1, 2, 3, ...., 14, 15, 0] -- > False otherwise True
    :return: True -- bord is not sorted
            False -- bord is sorted
    """
    return True


def print_bord(bord):
    print("15, 14, 13, 12\n 11, 10, 9, 8\n 7,  6,  5,  4\n 3,  1,  2,  _")


def move(bord, try_move):
    pass



#==============================
print_greeting()
bord = initialize_bord()
while check(bord):
    print_bord(bord)
    try_move = int(input("Number to move >>"))
    bord = move(bord, try_move)

print("Congratulation You have ordered the bord")
